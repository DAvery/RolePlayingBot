# https://github.com/Rapptz/discord.py/blob/async/examples/reply.py
from discord.ext import commands
import random
import discord

TOKEN = 'NDIxOTY5MDU5Nzk0NTE4MDM3.DYU9Kg.MeQ0yGSHwbrvxL4S6h0lxd5uiAk'
description = '''An example bot to showcase the discord.ext.commands extension
module.
There are a number of utility commands being showcased here.'''

prefix = '$';

commandList = ["roll"]


bot = commands.Bot(command_prefix='$', description=description)

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print("Bot Start Successful, enjoy Roleplaying fun")
    print('------')

@bot.event
async def on_message(message):
    #Classes and stuff for figuring what to do go here
#    trimmedMessage = message.content.strip();
#    channel = message.channel();
    print("on_message currently not working");
    await bot.process_commands(message);
            
    
    
#May not be used anymore
def checkForString(mainString, substringInside):
    index=-1;
    try:
        index = mainString.index(substringInside)
    except ValueError:
        return -1
#            print("Debug Print, the substring was not there")
    return index
    
#This should run commands take in options and stuff like that
def runCommand(command,optionsList):
    print("Something very wrong happened")
            
# Previous Code, how to do singular bot commands, on_message overwrites, prefer that anyway
@bot.command()
async def add(left : int, right : int):
     """Adds two numbers together."""
     await bot.say(left + right)
 
@bot.command()
async def roll(dice : str, *options : str):
     """Rolls a dice in NdN format."""
     index = checkForString(dice, "d");
     if index == 0 and index != -1:
         nRoll = 1;
         #Empty string is a thing?
         limit = int(dice.split('d')[1]);
        
     elif index != -2:
         nRoll, limit = map(int,dice.split('d'));
        
     #TODO Options, need to be coded for, but yeah, we got options   
     print(", ".join(options));

     result = ', '.join(str(random.randint(1, limit)) for r in range(nRoll))
     await bot.say(result)

@bot.command(description='For when you wanna settle the score some other way')
async def choose(*choices : str):
     """Chooses between multiple choices."""
     await bot.say(random.choice(choices))
 
@bot.command()
async def repeat(times : int, content='repeating...'):
     """Repeats a message multiple times."""
     for i in range(times):
         await bot.say(content)
 
@bot.command()
async def joined(member : discord.Member):
     """Says when a member joined."""
     await bot.say('{0.name} joined in {0.joined_at}'.format(member))

bot.run(TOKEN)